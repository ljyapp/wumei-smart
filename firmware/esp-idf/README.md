#### get-started项目：实现wifi和mqtt功能，对接系统的基本示例。
* 修改wifi.c文件中的 SSID和密码，连接自己wifi
* 修改mqtt.c文件中的mqtt信息，连接自己的mqtt代理服务


#### 环境
* 芯片采用ESP32S2，基于ESP-IDF v4.2版本开发。其他版本可能会有少许出入。
* [硬件接入文档看这里 >>>](https://gitee.com/kerwincui/wumei-smart/wikis/pages?sort_id=4203154&doc_id=1506495)